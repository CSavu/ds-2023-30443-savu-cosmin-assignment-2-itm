# ITM (Internet of Things Monitor)

## Overview
ITM (Internet of Things Monitor) is a monitoring service designed for IoT devices. It provides real-time insights and analytics to ensure the optimal performance and security of the Energy Management System.

## Getting Started

### Prerequisites

### Installation
1. Clone the ITM repository to your local machine.
   ```bash
   git clone https://gitlab.com/CSavu/itm.git
   cd itm
    ```
   
