package com.itm.filters;

public enum OperationPaths {
    GET_ALL_FOR_DEVICE_ON_DATE("/getAllForDeviceOnDate");

    private String path;

    OperationPaths(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
