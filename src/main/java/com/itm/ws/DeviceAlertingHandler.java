package com.itm.ws;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class DeviceAlertingHandler implements WebSocketHandler {

    // Map to store the association between session IDs and userIds
    private static final Map<String, WebSocketSession> sessionUserIdMap = new HashMap<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) {
        String userId = extractUserId(webSocketSession);
        if (userId != null) {
            log.info("Connection established with session id={} and userId={}", webSocketSession.getId(), userId);
            sessionUserIdMap.put(userId, webSocketSession);
        }
    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        log.info("Message received from session id={}", webSocketSession.getId());
        webSocketSession.sendMessage(new TextMessage("Hello from server"));
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) {
        log.error("Error occurred in session id={}", webSocketSession.getId(), throwable);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) {
        log.info("Connection closed with session id={}", webSocketSession.getId());
    }

    public void sendMessageToUser(String userId, String message) {
        WebSocketSession session = sessionUserIdMap.get(userId);
        if (session != null && session.isOpen()) {
            try {
                log.info("Sending message to userId={}", userId);
                session.sendMessage(new TextMessage(message));
            } catch (Exception e) {
                log.error("Error occurred while sending message to userId={}", userId, e);
            }
        }
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    private String extractUserId(WebSocketSession webSocketSession) {
        // Example: extracting userId from query parameters
        String userId = webSocketSession.getUri().getQuery();
        if (userId != null && userId.startsWith("userId=")) {
            return userId.substring("userId=".length());
        }
        return null;
    }
}
