package com.itm.adapters;

import com.itm.clients.UserManagementServiceClient;
import com.itm.dtos.AuthorizationResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UserManagementServiceAdapter {
    @Autowired
    private UserManagementServiceClient umsClient;

    public static String AUTH_TOKEN;

    // Generate a new auth token every 30 minutes
    @Scheduled(fixedDelay = 1800000)
    public void generateAuthToken() {
        AUTH_TOKEN = generateNewAuthToken();
        log.info("Generated new auth token: {}", AUTH_TOKEN);
    }

    public AuthorizationResponseDto verifyToken(String token) {
        return umsClient.verifyToken(token);
    }

    public String generateNewAuthToken() {
        return umsClient.authenticate().getToken();
    }
}
