package com.itm.configs;

import com.itm.filters.AuthorizationFilter;
import com.itm.filters.CORSFilter;
import com.itm.filters.RoleFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private CORSFilter corsFilter;
    @Autowired
    private AuthorizationFilter authorizationFilter;
    @Autowired
    private RoleFilter roleFilter;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .requiresChannel(channel -> channel.anyRequest().requiresSecure());
    }
}
