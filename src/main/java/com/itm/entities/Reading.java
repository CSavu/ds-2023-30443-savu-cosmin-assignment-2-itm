package com.itm.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
public class Reading {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reading_generator")
    @SequenceGenerator(name="reading_generator", sequenceName = "reading_seq", allocationSize=1)
    private Integer id;

    @NonNull
    private Integer deviceId;

    @NonNull
    private Long timestamp;

    @NonNull
    @JsonProperty("measuredValue")
    private Double measuredEnergyConsumption;
}
